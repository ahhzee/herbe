static const char *background_color = "#3e3e3e";
static const char *border_color = "#ffa500";
static const char *font_color = "#ececec";
static const char *font_pattern = "UW Ttyp0:pixelsize=16:antialias=true:autohint=true";
static const unsigned line_spacing = 5;
static const unsigned int padding = 15;

static const unsigned int width = 350;
static const unsigned int border_size = 2;
static const unsigned int pos_x = 36;
static const unsigned int pos_y = 42;

enum corners { TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT };
enum corners corner = TOP_RIGHT;

static const unsigned int duration = 8; /* in seconds */

#define DISMISS_BUTTON Button1
#define ACTION_BUTTON Button3
